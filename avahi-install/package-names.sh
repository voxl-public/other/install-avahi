#!/bin/bash

common_ipk=libavahi-common3_0.6.31-r11.1_aarch64.ipk
core_ipk=libavahi-core7_0.6.31-r11.1_aarch64.ipk
daemon_ipk=libdaemon0_0.14-r0_aarch64.ipk
avahi_ipk=avahi-daemon_0.6.31-r11.1_aarch64.ipk
nss_mdns_ipk=libnss-mdns_0.10-r7_aarch64.ipk
