#!/bin/bash

source package-names.sh

# Install Avahi and all dependencies in the correct order
opkg install $common_ipk
opkg install $core_ipk
opkg install $daemon_ipk
opkg install $avahi_ipk
opkg install $nss_mdns_ipk
